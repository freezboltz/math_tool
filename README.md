# Math Functions Script

This script contains several math functions that can be used for different calculations.
Functions

The available functions in the script are:

    `is_prime(num: int) -> bool`: Checks whether a given number is prime or not.
    `tetranacci_number(n: int) -> int`: Calculates the n-th Tetranacci number.
    `mersenne_number(n: int) -> int`: Calculates the n-th Mersenne number.
    `mersenne_prime_number(n: int) -> Optional[int]`: Computes the nth Mersenne prime number.
    `taylor(fn: Callable[[int, int], float], x: int, n: int, c: int) -> float`: Calculates the nth-degree Taylor polynomial approximation of a given function at a given point.

## Usage

The script can be executed from the command line with the following options:

    * -n or --number: The number to be used in the calculation.
    * -x or --x_variable: The point at which to approximate the function.
    * -c or --center_of_expansion: The center of expansion to be used in the Taylor series.
    * --function: The function to be executed. The available options are:
        - check_prime: Check whether a number is prime or not.
        - tetranacci_number: Calculate the n-th Tetranacci number.
        - mersenne_number: Calculate the n-th Mersenne number.
        - mersenne_prime_number: Compute the nth Mersenne prime number.
        - taylor: Calculate the nth-degree Taylor polynomial approximation of a given function at a given point.

# Example usage:

```python
python math_functions.py --function check_prime -n 7

#This will check whether 7 is a prime number or not.
```

```python
python math_functions.py --function tetranacci_number -n 10

#This will calculate the 10th Tetranacci number.
```

```python
python math_functions.py --function mersenne_number -n 7

#This will calculate the 7th Mersenne number.
```

```python
python math_functions.py --function mersenne_prime_number -n 7

#This will compute the 7th Mersenne prime number.
```


```python
python math_functions.py --function taylor -x 1 -n 3 --center_of_expansion 0
```

This will calculate the 3rd-degree Taylor polynomial approximation of the function sin(x) at the point x=1, with a center of expansion at c=0.
