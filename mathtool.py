#!/bin/python

import math
import argparse
from typing import (
        Optional,
        Sequence,
        Union,
        List,
        Callable
    )

def tetranacci_number(n: int) -> int:
    """Calculate the n-th Tetranacci number."""
    if n < 0:
        raise ValueError("n must be a non-negative integer")
    elif n == 0:
        return 0
    elif n <= 3:
        return 1
    else:
        return tetranacci_number(n-1) + tetranacci_number(n-2) + tetranacci_number(n-3) + tetranacci_number(n-4)

def factors_or_prime(num: int) -> Union[str, List[int]]:
    """
    Given an integer `num`, return a sorted list of its factors if it is composite,
    or the string "Prime number" if it is prime.

    Args:
        num: An integer to find the factors of or check if it's prime.

    Returns:
        If the number is less than 2, returns the string "Not applicable. Enter a number greater than or equal to 2."
        If the number is composite, returns a sorted list of its factors.
        If the number is prime, returns the string "Prime number".

    Raises:
        N/A

    Examples:
        >>> factors_or_prime(12)
        [1, 2, 3, 4, 6, 12]
        >>> factors_or_prime(17)
        "Prime number"


     >>> factors_or_prime(0)
        "Not applicable. Enter a number greater than or equal to 2."
    """
    # check if the number is less than 2
    if num < 2:
        return "Not applicable. Enter a number greater than or equal to 2."

    factors = []
    # loop from 1 to the number (inclusive) to find factors
    for i in range(1, num+1):
        if num % i == 0:
            factors.append(i)

    # check if the number has more than 2 factors
    if len(factors) > 2:
        return sorted(factors)
    else:
        return "Prime number"


def is_prime(num: int) -> bool:
    """
    This function takes an integer as input and returns True if the input number is prime,
    False otherwise.
    """
    if num <= 1:
        return False
    # Check for factors in range 2 to square root of the number
    for i in range(2, int(num**0.5) + 1):
        if num % i == 0:
            return False
    return True

def mersenne_number(n: int) -> int:
    return 2**n - 1

def mersenne_prime_number(n: int) -> Optional[int]:
    """
    Compute the nth Mersenne prime number.

    Args:
        n (int): The exponent for the Mersenne prime number.

    Returns:
        int: The nth Mersenne prime number, or None if the number is not prime or the Mersenne prime is not known.
    """
    if n < 2:
        return None

    # Check if n is prime using trial division.
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return None

    # Use Lucas-Lehmer primality test to check if 2^n - 1 is prime.
    s = 4
    mersenne = (1 << n) - 1
    for i in range(n - 2):
        s = ((s * s) - 2) % mersenne
    return mersenne if s == 0 else None

def taylor(fn: Callable[[int, int], float], x: int, n: int, c: int) -> float:
    """
    Calculates the nth-degree Taylor polynomial approximation of the given function at the given point.
    where:
        - `fn`: The function to approximate.
        - `x`: The point at which to approximate the function.
        - `n`: The number of terms to use in the Taylor series expansion.
        - `c`: Center of expansion
    """
    result = 0.0
    for i in range(n+1):
        result += fn(i, x) / math.factorial(i) * (x - c)**i
    return result

def fn(n: int, x: int) -> float:
    """
    Returns the nth derivative of sin(x) evaluated at x.

    Args:
        n (int): The order of the derivative to evaluate. Must be 0, 1, 2, or 3.
        x (int): The value at which to evaluate the nth derivative of sin(x).

    Returns:
        float: The value of the nth derivative of sin(x) evaluated at x.

    Raises:
        ValueError: If the order of the derivative is not one of 0, 1, 2, or 3.
    """
    if n == 0:
        return math.sin(x)
    elif n == 1:
        return math.cos(x)
    elif n == 2:
        return -math.sin(x)
    elif n == 3:
        return -math.cos(x)
    else:
        raise ValueError("Only derivatives up to order 3 are supported.")

def main(argv: Optional[Sequence[str]] = None) -> Optional[Union[str, List[int], bool, int, float]]:
    """
    Parses the command-line arguments and calls the appropriate function based on the input parameters.

    Args:
        argv (Optional[Sequence[str]]): A list of command-line arguments.

    Returns:
        Optional[Union[bool, int, float]]: The result of the called function, or None if no function was called.
    """
    parser = argparse.ArgumentParser(description='Calculates the nth-degree Taylor polynomial approximation of the given function at the given point.')
    parser.add_argument('-n', '--number', type = int, help='The number of terms to use in the Taylor series expansion.')
    parser.add_argument('-x', '--x_variable', type = int, help='The point at which to approximate the function.')
    parser.add_argument('-c', '--center_of_expansion', type = int, default=0, help='Center of expansion')
    parser.add_argument('--function', choices=['check_prime','tetranacci_number','mersenne_number', 'factors_or_prime', 'mersenne_prime_number','taylor'], help='choice for different function')
    args = parser.parse_args(argv)
    if args.function == 'check_prime':
        return is_prime(args.number)
    elif args.function == 'tetranacci_number':
        return tetranacci_number(args.number)
    elif args.function == 'mersenne_number':
        return mersenne_number(args.number)
    elif args.function == 'mersenne_prime_number':
        return mersenne_prime_number(args.number)
    elif args.function == 'factors_or_prime':
        return factors_or_prime(args.number)
    elif args.function == 'taylor':
        return taylor(fn, args.x_variable, args.number, args.center_of_expansion)
    return None

if __name__ == "__main__":
    print(main())
